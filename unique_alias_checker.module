<?php

/**
 * @file
 * Contains unique_alias_checker.module.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function unique_alias_checker_form_node_form_alter(array &$form, $form_state, $form_id) {
  if (_unique_alias_checker_is_applicable($form_state)) {
    $form['#validate'][] = 'unique_alias_checker_validate';
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function unique_alias_checker_form_taxonomy_term_form_alter(&$form, $form_state, $form_id) {
  if (_unique_alias_checker_is_applicable($form_state)) {
    $form['#validate'][] = 'unique_alias_checker_validate';
  }
}

/**
 * Checks whether alias checker validation should be applied for a form.
 *
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state object.
 *
 * @return bool
 *   TRUE if validation should be applied.
 */
function _unique_alias_checker_is_applicable($form_state) {
  $form_operation = $form_state->getFormObject()->getOperation();

  return $form_operation === 'default' || $form_operation === 'edit';
}

/**
 * Custom validation callback.
 *
 * Verify that when a user saves an entity it will not
 * generate a duplciate url_alias. Do not perform the check
 * if the Generate automatic URL alias checkbox is not checked.
 *
 * Implements hook_validate().
 */
function unique_alias_checker_validate(&$form, $form_state) {
  // Check if the "Generate automatic URL alias" checkbox is checked.
  if ($form_state->getValue('path')[0]['pathauto']) {

    // Build the entity object from the $form_state.
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = $form_state->getFormObject()->buildEntity($form, $form_state);

    // Check whether entity bundle is excluded from validation.
    $excluded_bundles_settings = \Drupal::config('unique_alias_checker.settings')->get('exclude_bundles');
    $entity_type_id = $entity->getEntityTypeId();

    if ($excluded_bundles_settings && array_key_exists($entity_type_id, $excluded_bundles_settings)) {
      $entity_type_bundles_settings = $excluded_bundles_settings[$entity_type_id];
      $bundle = $entity->bundle();

      if (array_key_exists($bundle, $entity_type_bundles_settings) && $entity_type_bundles_settings[$bundle]) {
        return;
      }
    }

    // Check whether duplicated alias is about to be created.
    $duplicateAlias = \Drupal::service('unique_alias_checker')->checkAlias($entity);

    if (!$duplicateAlias) {
      return;
    }

    $config = \Drupal::config('unique_alias_checker.settings');
    $error_msg = $config->get('error_msg') ?: t('The URL that would be
    generated with this content is already in use. Please update the content to
    generate a new URL alias.');

    $form_state->setErrorByName('path', $error_msg);
  }
}

/**
 * Implements hook_help().
 *
 * {@inheritdoc}
 */
function unique_alias_checker_help(string $routeName) {
  if ($routeName === 'help.page.unique_alias_checker') {
    /** @var \Drupal\Core\Extension\ExtensionPathResolver $resolver */
    $resolver = \Drupal::service('extension.path.resolver');
    $path = $resolver->getPath('module', 'unique_alias_checker');
    $output = file_get_contents($path . '/README.md');
    return '<pre>' . $output . '</pre>';
  }
  return '';
}
