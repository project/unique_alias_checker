# Unique Alias Checker

This module was created to prevent content managers from accidentally triggering pathauto's handling 
of duplicate url aliases through a form validator capable of detecting if a duplicate url alias would be generated on save.

Supported entity types:
- Nodes
- Taxonomy Terms

For a full description of the module, visit the
[project page](https://www.drupal.org/project/unique_alias_checker).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/unique_alias_checker).

## Table of contents

- Requirements
- Installation
- Configuration

## Requirements

This module requires the following modules:

- [Ctools](https://www.drupal.org/project/ctools)
- [Token](https://www.drupal.org/project/token)
- [PathAuto](https://www.drupal.org/project/pathauto)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

[Settings page](/admin/config/unique_alias_checker/unique_alias_checker_settings).

### Error Message
Customize the error message to display on detection of a duplicative alias.

### Exclude bundles from validation (1.3.x)
Exclude specified bundle types from validation.
