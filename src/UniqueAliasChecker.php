<?php

namespace Drupal\unique_alias_checker;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\pathauto\PathautoGenerator;

/**
 * Provides a duplicate alias checker.
 */
class UniqueAliasChecker extends PathautoGenerator {

  /**
   * Pass in entity object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Pass in an $entity object with $form_state values to check to see if the
   *   url generated on save would be unique.
   */
  public function checkAlias(EntityInterface $entity) {
    // Retrieve and apply the pattern for this content type.
    $pattern = $this->getPatternByEntity($entity);
    if (empty($pattern)) {
      return NULL;
    }
    $nid = $entity->id();

    if (is_numeric($nid)) {
      $internalPath = $entity->toUrl()->getInternalPath();
      $source = '/' . $internalPath;
    }
    else {
      $source = NULL;
    }

    $langcode = $entity->language()->getId();

    // Core does not handle aliases with language Not Applicable;.
    if ($langcode == LanguageInterface::LANGCODE_NOT_APPLICABLE) {
      $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    }

    // Build token data.
    $data = [
      $this->tokenEntityMapper->getTokenTypeForEntityType($entity->getEntityTypeId()) => $entity,
    ];

    // Replace tokens with entity values.
    $alias = $this->token->replace($pattern->getPattern(), $data, [
      'clear' => TRUE,
      'callback' => [$this->aliasCleaner, 'cleanTokenValues'],
      'langcode' => $langcode,
      'pathauto' => TRUE,
    ], new BubbleableMetadata());

    $alias = $this->aliasCleaner->cleanAlias($alias);

    // If we have arrived at an empty string, discontinue.
    if (!mb_strlen($alias)) {
      return NULL;
    }

    // Check if this is a unique alias.
    $original_alias = $alias;
    $this->aliasUniquifier->uniquify($alias, $source, $langcode);
    if ($original_alias != $alias) {
      return TRUE;
    }
  }

}
