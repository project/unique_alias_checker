<?php

namespace Drupal\unique_alias_checker\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure Unique Alias Checker settings.
 */
class UniqueAliasCheckerSettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The entity type build info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unique_alias_checker_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'unique_alias_checker.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->config('unique_alias_checker.settings');

    $form['#tree'] = TRUE;

    if ($settings->get('error_msg')) {
      $default_error_msg = $settings->get('error_msg');
    }
    else {
      $default_error_msg = 'The URL that would be generated with this content is already in use. Please update the content to generate a new URL alias.';
    }

    $form['unique_alias_checker_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Settings'),
      '#open' => TRUE,
      'error_msg' => [
        '#type' => 'textarea',
        '#title' => $this->t('Error Message'),
        '#description' => $this->t('Change this text to customize the error message to be
        displayed when a duplicate alias is detected.'),
        '#default_value' => $default_error_msg,
      ],
    ];

    $form['exclude_bundles'] = [
      '#type' => 'details',
      '#title' => $this->t('Exclude bundle settings'),
      '#description' => $this->t('Select which bundles should be ignored during path alias checking.'),
    ];

    $exclude_bundles = $settings->get('exclude_bundles');
    $form['exclude_bundles']['node'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Node bundles'),
      '#options' => $this->getEntityTypeBundleOptions('node'),
      '#default_value' => $exclude_bundles ? $exclude_bundles['node'] : [],
    ];

    $form['exclude_bundles']['taxonomy_term'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Taxonomy term bundles'),
      '#options' => $this->getEntityTypeBundleOptions('taxonomy_term'),
      '#default_value' => $exclude_bundles ? $exclude_bundles['taxonomy_term'] : [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns the entity type bundles list.
   *
   * @param string $entity_type
   *   The entity type for which to retrieve the bundles.
   *
   * @return array
   *   The bundles list.
   */
  protected function getEntityTypeBundleOptions($entity_type) {
    $options_list = [];

    foreach ($this->entityTypeBundleInfo->getBundleInfo($entity_type) as $bundle_id => $bundle_info) {
      $options_list[$bundle_id] = $bundle_info['label'];
    }

    return $options_list;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('unique_alias_checker.settings');

    $config
      ->set('error_msg', $form_state->getValue('unique_alias_checker_settings')['error_msg'])
      ->set('exclude_bundles', $form_state->getValue('exclude_bundles'))
      ->save();
  }

}
